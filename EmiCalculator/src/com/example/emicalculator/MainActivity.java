package com.example.emicalculator;


import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;


public class MainActivity extends Activity implements OnClickListener {
	EditText principleEditText, monthsEdit,rateEdit;
	Button addButton;
	TextView resultText,totalPayment,totalInterestPayable;
	double monthlyEmi,totalPaymen,totalPayableInterest;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		principleEditText = (EditText) findViewById(R.id.amount_edittext);
		monthsEdit = (EditText) findViewById(R.id.year_edittext);
		rateEdit = (EditText) findViewById(R.id.ratio_edittext);
		addButton = (Button) findViewById(R.id.add_button);
		resultText = (TextView) findViewById(R.id.resultText);
		totalPayment = (TextView) findViewById(R.id.total_payment);
		totalInterestPayable = (TextView) findViewById(R.id.total_interest_payable);
		addButton.setOnClickListener(this);
		
		
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.add_button:
			if(principleEditText.getText().toString().length() >0&&monthsEdit.getText().toString().length() >0&&rateEdit.getText().toString().length() >0){
				resultText.setVisibility(View.VISIBLE);
				totalPayment.setVisibility(View.VISIBLE);
				totalInterestPayable.setVisibility(View.VISIBLE);
				calculateEmi();
				
			}else{
				
				Toast.makeText(getApplicationContext(), "Fields should not be empty", Toast.LENGTH_SHORT).show();
			}
			break;

		default:
			break;
		}

	}
	public  void calculateEmi()
	{
		// formula for calculate emi  P � r � (1 + r)^ n  /  ( (1 + r) ^ n - 1)
		// r = (r / 12) / 100 ;
		
		double principle= Double.parseDouble(principleEditText.getText().toString()); 
		double rate = Double.parseDouble(rateEdit.getText().toString()) ; // 
		rate= (rate / 12) / 100;
		double firstHalf = (principle *rate )* Math.pow((1 + rate), Integer.parseInt(monthsEdit.getText().toString())); 
		double secondHalf = Math.pow((1 + rate), Integer.parseInt(monthsEdit.getText().toString())) - 1;
		 monthlyEmi = Math.round(firstHalf /secondHalf );
	resultText.setText("Loan EMI : "+monthlyEmi);
	 totalPaymen = Math.round((monthlyEmi * Integer.parseInt(monthsEdit.getText().toString())));
	 totalPayableInterest = Math.round(Math.abs((monthlyEmi * Integer.parseInt(monthsEdit.getText().toString())) - principle));
	totalPayment.setText(" Total Payment(Principal + Interest) : " + totalPaymen);
	totalInterestPayable.setText(" Total Interest Payable : "+ totalPayableInterest);
	}
	//save data if orientation changes
	@Override
	protected void onSaveInstanceState(Bundle outState) {
		
		super.onSaveInstanceState(outState);
		outState.putDouble("monthlyemi", monthlyEmi);
		outState.putDouble("totalpayment", totalPaymen);
		outState.putDouble("totalpayableinterest", totalPayableInterest);
		
	}
	//restore data  if orientation changes
	@Override
	protected void onRestoreInstanceState(Bundle savedInstanceState) {
		super.onRestoreInstanceState(savedInstanceState);
		resultText.setVisibility(View.VISIBLE);
		totalPayment.setVisibility(View.VISIBLE);
		totalInterestPayable.setVisibility(View.VISIBLE);
		monthlyEmi = savedInstanceState.getDouble("monthlyemi");
		totalPaymen= savedInstanceState.getDouble("totalpayment");
		totalPayableInterest = savedInstanceState.getDouble("totalpayableinterest");
		resultText.setText("Loan EMI : "+monthlyEmi);
		totalPayment.setText(" Total Payment(Principal + Interest) : " +totalPaymen);
		totalInterestPayable.setText(" Total Interest Payable : "+ totalPayableInterest);
		
	}

	
}
